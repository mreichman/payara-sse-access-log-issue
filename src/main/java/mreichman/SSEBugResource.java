package mreichman;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.sse.OutboundSseEvent;
import javax.ws.rs.sse.Sse;
import javax.ws.rs.sse.SseBroadcaster;
import javax.ws.rs.sse.SseEventSink;
import java.util.UUID;

@ApplicationScoped
@Path("sse")
public class SSEBugResource {
    private SseBroadcaster sseBroadcaster;
    private OutboundSseEvent.Builder eventBuilder;

    @Context
    public void setSse(Sse sse) {
        this.eventBuilder = sse.newEventBuilder();
        this.sseBroadcaster = sse.newBroadcaster();
    }

    @GET
    @Produces(MediaType.SERVER_SENT_EVENTS)
    public void register(@Context SseEventSink eventSink) {
        eventSink.send(getEvent("Broadcaster Connected"));
        sseBroadcaster.register(eventSink);
    }

    @POST
    public Response postMessage(final String message) {
        sseBroadcaster.broadcast(getEvent(message));
        return Response.noContent().build();
    }

    private OutboundSseEvent getEvent(final String message) {
        return eventBuilder
                .mediaType(MediaType.TEXT_PLAIN_TYPE)
                .id(UUID.randomUUID().toString())
                .name("TEST")
                .reconnectDelay(10000)
                .data(message)
                .build();
    }
}
