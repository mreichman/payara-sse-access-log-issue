# payara-sse-access-log-issue

Requires JDK 17, tested with Payara Micro Community 5.2022.2

## Build

```bash
$ ./gradlew clean war
```

## Deploy with Payara Micro

### With Access Logs (shows warnings on SSE connection / broadcast)
```bash
$ java -jar payara-micro-5.2022.2.jar --accesslog logs --port 9999 --deploy build/libs/payara-sse-access-log-issue.war
```

### Without Access Logs (no warnings on SSE connection / broadcast)
```bash
$ java -jar payara-micro-5.2022.2.jar --port 9999 --deploy build/libs/payara-sse-access-log-issue.war
```

## Test

Open browser to `http://localhost:9999/payara-sse-access-log-issue/` and click `Post Message` button.